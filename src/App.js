import "./App.css";
import React, { Component } from "react";
import Contacts from "./containers/contacts/Contacts";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Contacts />
      </div>
    );
  }
}

export default App;
