import React from "react";

const fetchImg = (props) => {

    if (props.src !== null) {
        return (<img src={props.src} alt="Contact's avatar"/>)
    } else {
        return null
    }
};

export default fetchImg;