import React from "react";
import FetchImg from "./fetchImg/FetchImg";

const checkbox = (props) => {
  return (
    <div class="main-container__list__item__checkbox">
    <input id="option1" name={props.contactId} type="checkbox"/>
    <label for="option1">Option One</label>
  </div>
  );
};

export default checkbox;