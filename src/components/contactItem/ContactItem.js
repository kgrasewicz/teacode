import React from "react";
import FetchImg from "./fetchImg/FetchImg";

const contactItem = (props) => {



  return (
    <li className="main-container__list__item">
        <label className="main-container__list__item__label inactive" htmlFor={props.data.id} >
        <div className="main-container__list__item__label__avatar">
            <h1>{props.data.first_name[0] + props.data.last_name[0]}</h1>
            <FetchImg src={props.data.avatar} />
        </div>
        <div className="main-container__list__item__label__details">
            <h3>{props.data.first_name + " " + props.data.last_name}</h3>
            <h4>{props.data.email}</h4>
        </div>
        <input className="main-container__list__item__label__checkbox" id={props.data.id} onChange={props.handler} name={props.contactId} type="checkbox"/>
        </label>
    </li>
  );
};

export default contactItem;