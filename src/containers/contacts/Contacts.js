import React, { Component } from "react";
import Search from "../../components/search/Search";
import axios from "axios";
import _ from "lodash";
import ContactItem from "../../components/contactItem/ContactItem";

class Contacts extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      filteredContacts: [],
      checkContacts: [],
    };

    this.props = props;
  }

  componentDidMount = () => {
    this._isMounted = true;
    this.fetchData();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  fetchData = () => {
    axios
      .get(
        `https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`
      )
      .then((response) => {
        if (this._isMounted) {
          this.setState({
            contacts: _.orderBy(response.data, "last_name", "asc"),
            pageNumber: this.state.pageNumber + 1
          });
          this.setState({ filteredContacts: this.state.contacts });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({ contacts: "Error" });
      });
  };

  searchEngine = () => {
    let query = document.querySelector(".main-container__search__input").value;

    let filtered = this.state.contacts.filter((filteredContact) => {
      if (
        filteredContact.first_name.toLowerCase() !== query.toLowerCase() &&
        filteredContact.last_name.toLowerCase() !== query.toLowerCase()
      ) {
        return false;
      }

      return true;
    });

    this.setState({ filteredContacts: filtered }, () => {
      for (var i = 0; i < this.state.checkContacts.length; i++) {
        if (
          document.getElementById(`${this.state.checkContacts[i]}`) !== null
        ) {
          document.getElementById(
            `${this.state.checkContacts[i]}`
          ).checked = true;
        }
      }
    });
    document
      .querySelector(".main-container__search__toggle")
      .classList.add("active");
  };

  clearFilters = () => {
    this.setState({ filteredContacts: this.state.contacts }, () => {
      for (var i = 0; i < this.state.checkContacts.length; i++) {
        if (
          document.getElementById(`${this.state.checkContacts[i]}`) !== null
        ) {
          document.getElementById(
            `${this.state.checkContacts[i]}`
          ).checked = true;
        }
      }
    });
    document.querySelector(".main-container__search__input").value = "";
    document
      .querySelector(".main-container__search__toggle")
      .classList.remove("active");
  };

  checkHandler = (e) => {
    let checked = [];

    checked = this.state.checkContacts;
    let id = e.target.id;

    const index = checked.findIndex((x) => x === id);

    if (index === -1) {
      checked.push(id);
    } else {
      checked.splice(index, 1);
    }

    this.setState(
      { checkContacts: checked },
      console.log(this.state.checkContacts)
    );

    e.target.parentElement.classList.toggle("active");
    e.target.parentElement.classList.toggle("inactive");
  };

  render() {
    if (this.state.contacts.length === 0) {
      return (
        <div className="main-container">
          <div className="main-container__title">
            <h1>Contacts</h1>
          </div>
          <div className="main-container__loading">
            <h2>Loading...</h2>
          </div>
        </div>
      );
    } else if (this.state.contacts === "Error") {
      return (
        <div className="main-container">
          <div className="main-container__title">
            <h1>Contacts</h1>
          </div>
          <div className="main-container__error">
            <h2>Oops..., something went wrong.</h2>
            <h3>Reload the page or contact our support team.</h3>
          </div>
        </div>
      );
    }

    const contacts = this.state.filteredContacts.map((contact) => (
      <ContactItem
        key={contact.id}
        data={contact}
        handler={(e) => this.checkHandler(e)}
      />
    ));

    return (
      <div className="main-container">
        <div className="main-container__title">
          <h1>Contacts</h1>
        </div>
        <Search handler={this.searchEngine} clearHandler={this.clearFilters} />
        <ul className="main-container__list">
            {contacts}
            </ul>
      </div>
    );
  }
}

export default Contacts;
